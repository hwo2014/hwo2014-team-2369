package driver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import noobbot.State;
import model.Coordinate;
import model.Curve;
import model.DrivingCommand;
import model.Turbo;
import model.DrivingCommand.CommandType;
import model.RaceCar;
import model.RacePart;
import model.RacePart.RoadType;
import model.DrivingCommand.LaneChageDirection;

public class Driver {

    private Logger logger = LoggerFactory.getLogger("Bot.driver");
    
    /* Physical properties / parameters */

    // F - alpha * v^2 = m * a  | alpha * v^2 = wind resistance, friction etc
    //							| m = 1
    //							| F =  gamma * throttle value = gamma * T
    // alpha = (a1 - a2)/(v2^2 - v1^2)
    // gamma = (alpha * v_lim * v_lim) | v_lim = terminal velocity
    // 								   | T = throttle value
	
    double alpha;
    double beta;
    double gamma;
    double epsilon;
    
    double maxSafeAngle = 0;
    
    /* Testing parameters */
	private static final int ACC_TEST_PERIOD = 30;
	private static final double ACC_TEST_THROTTLE = 0.40;
	private static final double TERMINAL_VELOCITY_TEST_THROTTLE = ACC_TEST_THROTTLE;
	private static final double TERMINAL_VELOCITY_TEST2_THROTTLE = 0.3;
	private static final double CRASH_TEST_START_THROTTLE = 0.5;

    /* Variables for test states */
	private boolean testingDone = false;
	private double a1,a2,v1,v2;

	boolean terminalVelocityTestInProggress  = false;
	int termVelocityTestPhase1end = 0;
	double terminalVelocity1 = -1;
	double terminalVelocity2 = -1;
	
	boolean crashTestInProggress = false;
	
	private Map<Integer, Integer> lanePlan = new HashMap<Integer, Integer>();
    
	/* driving parameters */
    private double MAX_CENTRIFUGAL_ACCELERATION = 0.5;
    private double MAX_CFA_SUM = 20;

    private double turboFactor = 1;
    private double targetSpeed;
    private double current_cfa_sum = 0;
    private int crashCount = 0;
    
    public double getTargetSpeed() {
    	return targetSpeed;
    }
    
    private int createLanePlan(State state, int startingIndex) {
    	int secondSwitchIndex = state.getNextSwitchIndex(startingIndex + 1);
		
		Coordinate distance = state.getDistance(startingIndex, secondSwitchIndex);
		double minDistance = Double.MAX_VALUE;
		int minLaneIndex = -1;
		for(Integer lane : distance.getLanes()) {
			if(distance.getValue(lane) < minDistance) {
				minDistance = distance.getValue(lane);
				minLaneIndex = lane;
			}
		}
		
		int end = secondSwitchIndex;
		if(secondSwitchIndex < startingIndex) {
			 end = state.getTrackLength();
		}
		for(int i=startingIndex; i<end; i++) {
			lanePlan.put(i, minLaneIndex);
		}
		// Handle overflow
		if(secondSwitchIndex < startingIndex) {
			for(int i=0; i<secondSwitchIndex; i++) {
				lanePlan.put(i, minLaneIndex);
			}
		}
		
		return secondSwitchIndex;
    }
    
    public Driver(State state) {
    	logger.debug("track size " + state.getTrackLength());
    	int firstSwitchIndex = state.getNextSwitchIndex(0);
    	int startIndex = firstSwitchIndex;
    	do {
    		startIndex = createLanePlan(state, startIndex);
    	} while(startIndex != firstSwitchIndex);
    	logger.debug("Lane plan:" + lanePlan);
    }
	
    public void crashed(State state) {
    	
		double crash_centrifugalAcceleration = state.getCurrentRadAcceleration();
		
		// If cfa == 0, we are past the curve, so if previous piece is curve, assume
		// it caused the crash and approximate cfa from that.
		if(crash_centrifugalAcceleration == 0) {
			RacePart currenPart = state.getCurrentRacePart();
			RacePart prevPart = state.getRacePart(currenPart.getIndex() - 1);
			if(prevPart.getType().equals(RoadType.Curve)) {
				Curve c = (Curve)prevPart;
				crash_centrifugalAcceleration = (state.getCurrentSpeed() * state.getCurrentSpeed()) / c.getRadius(lanePlan.get(c.getIndex()));
				logger.info("Crash: approximating cfa");
			}
		}
		// If cfa == 0, don't update criteria 
		// if speed > 10 asume we have used turbo, and ignore values
		if(crash_centrifugalAcceleration != 0 && state.getCurrentSpeed() < 9 && crash_centrifugalAcceleration < 1) {
			logger.info("Crash: cetrifugal acceleration = " + crash_centrifugalAcceleration);
			double curveTotalLen = getNextCurveLength(state, state.getNextCurve().getIndex());
			double curveDifficulty = getCurveDifficulty(curveTotalLen);
			logger.info("Crash: curve difficulty: " + curveDifficulty);
			logger.info("Crash: angle = " + state.getCurrentAngle());
	
			MAX_CENTRIFUGAL_ACCELERATION =
					((MAX_CENTRIFUGAL_ACCELERATION * crashCount) + crash_centrifugalAcceleration / curveDifficulty) / (crashCount + 1);
			
			maxSafeAngle = ((maxSafeAngle * crashCount) + Math.abs(state.getCurrentAngle()) ) / (crashCount + 1);
	    	
			logger.info("Crash: MAX_CENTRIFUGAL_ACCELERATION = " + MAX_CENTRIFUGAL_ACCELERATION);
			logger.info("Crash: angle = " + maxSafeAngle);
			
			double crashCfaSum = current_cfa_sum;
			// MAX_CFA_SUM = ((MAX_CFA_SUM) * crashCount + crashCfaSum) / (crashCount + 1);
			logger.info("Crash: cfa_sum=" + crashCfaSum);
			logger.info("Crash: Max cfa sum=" + MAX_CFA_SUM);
			current_cfa_sum = 0;
	    	crashCount++;
		} else {
			logger.warn("Crash with cfa=0");
		}
    	crashTestInProggress = false;
    }
    
    private double getThrottleForTargetSpeed(State state, double targetSpeed) {
    	if((state.getCurrentSpeed() - targetSpeed) < - 0.5) {
    		return 1;
    	} else if((state.getCurrentSpeed() > targetSpeed)) {
    		return 0;
    	} else {
    		double res =  (-epsilon + Math.sqrt(epsilon*epsilon - 4 * gamma * -(alpha*targetSpeed*targetSpeed)))/(2*gamma);
    		return res/turboFactor;
    	}	
    }
    
    private double getDeaccelerationTime(double startSpeed, double endSpeed) {
    	return (1-(endSpeed/startSpeed))/(alpha * endSpeed);
    }
    
    private double getDeaccelerationLength(double startSpeed, double endSpeed) {
    	double deaccelerationTime = getDeaccelerationTime(startSpeed, endSpeed);
    	return Math.log(alpha * startSpeed * deaccelerationTime + 1)/alpha;
    	
    }
    
	private DrivingCommand testDrive(State state) {
		// These need some checks so that crashes don't break results.
		double newThrottle = state.getThrottleValue();
		int currentTick = state.getCurrentTick();
		if(currentTick < ACC_TEST_PERIOD) {
			newThrottle = ACC_TEST_THROTTLE;
		} else if(currentTick == ACC_TEST_PERIOD) {

			v1 = state.getCurrentSpeed();
			// a = v_delta / t_delta
			a1 = state.getCurrentAcceleration();
			
			newThrottle = ACC_TEST_THROTTLE;
		} else if(currentTick > ACC_TEST_PERIOD && currentTick < ACC_TEST_PERIOD*2) {
			newThrottle = ACC_TEST_THROTTLE;
		} else if(currentTick == 2*ACC_TEST_PERIOD) {	
			v2 = state.getCurrentSpeed();
			a2 = state.getCurrentAcceleration();
			
			logger.info("v1=" + v1 + " v2=" + v2 + " a1=" + a1 + " a2=" + a2);
			alpha = (a1 - a2)/(v2*v2 - v1*v1);
			logger.info("Alpha=" + alpha);
			newThrottle = TERMINAL_VELOCITY_TEST_THROTTLE;
			logger.info("Starting terminal velocity test");
			terminalVelocityTestInProggress = true;
			
		} else { 
			if(terminalVelocityTestInProggress) {
				if(Math.abs(state.getCurrentAcceleration()) < 0.001) {
					if(terminalVelocity1 < 0) {
						terminalVelocity1 = state.getCurrentSpeed();
						logger.info("Terminal velocity1: " + terminalVelocity1);
						termVelocityTestPhase1end = state.getCurrentTick();
						newThrottle = TERMINAL_VELOCITY_TEST2_THROTTLE;
					} else if(terminalVelocity2 < 0 && (termVelocityTestPhase1end + 20) < state.getCurrentTick()) {
						terminalVelocity2 = state.getCurrentSpeed();
						logger.info("Terminal velocity2: " + terminalVelocity2);
						double mult=  alpha / (TERMINAL_VELOCITY_TEST_THROTTLE*TERMINAL_VELOCITY_TEST2_THROTTLE*(TERMINAL_VELOCITY_TEST2_THROTTLE - TERMINAL_VELOCITY_TEST_THROTTLE));
						epsilon = mult * (TERMINAL_VELOCITY_TEST2_THROTTLE*TERMINAL_VELOCITY_TEST2_THROTTLE*terminalVelocity1 - TERMINAL_VELOCITY_TEST_THROTTLE*TERMINAL_VELOCITY_TEST_THROTTLE*terminalVelocity2);
						gamma = (alpha * terminalVelocity1 * terminalVelocity1 - epsilon * TERMINAL_VELOCITY_TEST_THROTTLE) / (TERMINAL_VELOCITY_TEST_THROTTLE*TERMINAL_VELOCITY_TEST_THROTTLE); 
						logger.info("Alpha=" + alpha);
						logger.info("Gamma=" + gamma);
						logger.info("Epsilon=" + epsilon);
							
						logger.info("Calculater v_max=" + Math.sqrt((gamma + epsilon)/alpha));
	
						terminalVelocityTestInProggress = false;
						// logger.info("Starting crash test");
						// crashTestInProggress = true;
						// newThrottle = CRASH_TEST_START_THROTTLE;
						testingDone = true;
					}
				} 
			} else if(crashTestInProggress) {
				RacePart currentPart = state.getCurrentRacePart();

				if(currentPart.getType().equals(RoadType.Curve)) {
					newThrottle += 0.02;
				} else {
					newThrottle = CRASH_TEST_START_THROTTLE;
				}
			} else {
				logger.info("Testing done");
				testingDone = true;
			}
		}
		
		if(state.getThrottleValue() == newThrottle) {
			// throttle value has not changed -> return null == no command
			return null;
		} else {
			DrivingCommand result = new DrivingCommand(CommandType.Throttle);
			result.setThrottleValue(newThrottle);
			return result;
		}
	}
	
	private int getCurveStartIndex(State state, int curvePartIndex) {
		int result = curvePartIndex;
		while(state.getRacePart(result - 1).getType().equals(RoadType.Curve)) {
			result--;
		}
		return result;
	}
	
	
	private double getNextCurveLength(State state, int curveStartIndex) {
		double result = 0;
		RacePart part = state.getRacePart(curveStartIndex);
		while(part.getType().equals(RoadType.Curve)){
			result += part.getLength(lanePlan.get(part.getIndex()));
			part = state.getRacePart(part.getIndex() + 1);
		}
		return result;
	}
	
	private double getCurveDifficulty(double curveLength) {
		double value = 3*Math.pow( -((curveLength - 300)/600) , 3) + 1;
		return Math.max(value, 0.1);
	}
	
	private double getTargetSpeedForCurve(State state, int curvePartIndex) {
		double result = Double.MAX_VALUE;
		int curveStartIndex = getCurveStartIndex(state, curvePartIndex);
		
		double curveTotalLength = getNextCurveLength(state, curveStartIndex);
		double curveDifficulty = getCurveDifficulty(curveTotalLength);
		
		RacePart part = state.getRacePart(curvePartIndex);
		
		while(part.getType().equals(RoadType.Curve)) {
			int partLaneIndex = lanePlan.get(part.getIndex());
			double partRadius = ((Curve)part).getRadius(partLaneIndex);
			// double part_cfa = (MAX_CFA_SUM * part.getLength(partLaneIndex)) / curveTotalLength;
			// double part_targetSpeed = (part_cfa * ((Curve)part).getRadius(partLaneIndex))/part.getLength(partLaneIndex);
			double part_targetSpeed = curveDifficulty * Math.sqrt(MAX_CENTRIFUGAL_ACCELERATION*0.85*partRadius);
		
			if(part_targetSpeed < result) {
				result = part_targetSpeed;
			}
			part = state.getRacePart(part.getIndex() + 1);
		} 
		 
		return result;
	}
	 
	private DrivingCommand normalDrive(State state) {
			
		Curve curve = state.getNextCurve();

		targetSpeed = getTargetSpeedForCurve(state, curve.getIndex());
		double newThrottle = getThrottleForTargetSpeed(state, targetSpeed);
		
		double curveDistance = state.getDistanceToNextCurve();

		if(curveDistance > 0) {
			if(state.getCurrentSpeed() > targetSpeed) {
				double deaccLenght = getDeaccelerationLength(state.getCurrentSpeed(), targetSpeed);
				// length calculation doesn't seem to work right at high speed
				// add some tolerance there
				if(state.getCurrentSpeed() > 9) {
					deaccLenght *= 1.8;
				}
				if(state.getDistanceToNextCurve() > deaccLenght + 5) {
					targetSpeed = 10;
					newThrottle = 1;
				} else {
					newThrottle = 0;
				}
			} else {
				// We are out of curve, but angle is high/increasing, run with half speed until stabilized
				if(Math.abs(state.getCurrentAngle()) < 20 && (state.getCurrentAngle() * state.getCurrentAngleDelta() ) >= 0) {
					targetSpeed = 10;
					newThrottle = 1;
				} else {
					// Out of curve and stabile -> full throttle
					targetSpeed = 10;
					newThrottle = 0.5;
				}
			}
		}

		if(state.getCurrentSpeed() > 14 && newThrottle == 1) {
			newThrottle = 0.5;
		}
		
		if(curveDistance>400 && newThrottle > 0){
			Turbo turbo = state.getAndUseTurbo();
			if(turbo != null){
				return new DrivingCommand(CommandType.UseTurbo); 
			}
		}
		
		if(newThrottle != state.getThrottleValue()) {
			DrivingCommand command = new DrivingCommand(CommandType.Throttle);
			command.setThrottleValue(newThrottle);
			return command;
		} else {
			return null;
		}
	}
	
	private DrivingCommand handleLaneChange(State state) {
		int nextLaneIndex = lanePlan.get(state.getNextSwitchIndex(state.getCurrentRacePart().getIndex()));
		
		boolean rushLeft = false;
		boolean rushRight = false;
		boolean rushCurrent = false;
		ArrayList<RaceCar> allCars = (ArrayList<RaceCar>) state.getAllCars();
		for(RaceCar car : allCars){
			if(state.getCurrentLane() == car.getLane() &&
					car.getPosition()>state.getOwnCar().getPosition() &&
					car.getPosition()<state.getOwnCar().getPosition()+100){
				rushCurrent = true;
			}else if(state.getCurrentLane() + 1 == car.getLane() &&
					car.getPosition()>state.getOwnCar().getPosition() &&
					car.getPosition()<state.getOwnCar().getPosition()+100){
				rushRight = true;
			}else if(state.getCurrentLane() - 1 == car.getLane() &&
					car.getPosition()>state.getOwnCar().getPosition() &&
					car.getPosition()<state.getOwnCar().getPosition()+100){
				rushLeft = true;
			}
		}
		
		if(rushCurrent){
			logger.info("rush in current lane!");
			if(!rushRight && state.getCurrentLane()+1<state.getLaneCount()){
				DrivingCommand result = new DrivingCommand(CommandType.LaneChange);
				result.setLaneChange(LaneChageDirection.RIGHT);
				logger.info("change lane to right!");
				return result;
			}else if(!rushLeft && state.getCurrentLane()-1>=0){
				DrivingCommand result = new DrivingCommand(CommandType.LaneChange);
				result.setLaneChange(LaneChageDirection.LEFT);
				logger.info("change lane to left!");
				return result;
			}
			
		}else{
			
			if(nextLaneIndex != state.getCurrentLane()) {
				DrivingCommand result = new DrivingCommand(CommandType.LaneChange);
				if(state.getLaneOffset(state.getCurrentLane()) < state.getLaneOffset(nextLaneIndex)) {
					result.setLaneChange(LaneChageDirection.RIGHT);
				} else {
					result.setLaneChange(LaneChageDirection.LEFT);
				}
				return result;
			}
		}
		return null;
	}
	
	public DrivingCommand getCommand(State state) {
		if(state.getCurrentAngle() * state.getCurrentAngleDelta() < 0 && current_cfa_sum > 0) {
			logger.info("MAX angle reached " + (state.getCurrentAngle() + state.getCurrentAngleDelta()) + " with cfa_sum=" + current_cfa_sum);
			current_cfa_sum = 0;
		} else if (state.getCurrentAngle() * state.getCurrentAngleDelta() >= 0) {
			if(state.getCurrentAngleDelta() != 0) { 
				current_cfa_sum += state.getCurrentRadAcceleration();
			}
		}
				
		if(!state.isCrashed()) {
			
			DrivingCommand result = null;
			
			/* Sometimes we get out of sync, we think that current throttle > 0, but 
			 * actual throttle value is 0 and car doesn't move. If throttle > 0 and
			 * we are not accelerating send throttle command again
			 */
			if(state.getThrottleValue() > 0 && state.getCurrentAcceleration() == 0 ) {
				result = new DrivingCommand(DrivingCommand.CommandType.Throttle);
				result.setThrottleValue(state.getThrottleValue());
				return result;
			}
			
			if(!testingDone) {
				result = testDrive(state);
			} else {
				result = normalDrive(state);
			}
			
			if(result == null) {
				result = handleLaneChange(state);
			}
			
			return result;
		} else {
			return null;
		}
	}

	public void setTurboEnded() {
		turboFactor = 1;
	}
}
