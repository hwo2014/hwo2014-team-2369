package driver;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import noobbot.State;
import model.Curve;
import model.DrivingCommand;
import model.DrivingCommand.CommandType;
import model.Stateinfo;
import model.Turbo;

public class Driver2 {
	private Logger logger = LoggerFactory.getLogger("Bot.driver");
    
    private static final double MAX_CENTRIFUGAL_ACCELERATION = 0.52;

	private double limitThrottle(double val) {
		if(val < 0) {
			return 0;
		}
		if(val > 1) {
			return 1;
		}
		return val;
	}
	
	private double changeThrottle(double throttle, double delta){
		if(throttle <= 0){
			return delta;
		}else{
			if(delta<0){
				return throttle * ( 1+delta*1.1);
			}
			return throttle * ( 1+delta*0.4);
		}
	}
	
	 
	private DrivingCommand testDrive(State state) {
		
		double distToNextCurve = state.getDistanceToNextCurve();
		
		ArrayList<Curve> curves = state.getNextCurves(3);
		double nextCurveR = 99999;
		for(Curve curve : curves){
			double r = curve.getRadius(state.getCurrentLane());
			if(r<nextCurveR){
				nextCurveR = r;
			}
		}
		double optimalSpeed = Math.sqrt(MAX_CENTRIFUGAL_ACCELERATION*nextCurveR);
		double deltaSpeed = optimalSpeed - state.getCurrentSpeed();
		
		double newThrottle = state.getThrottleValue();
		
		if(distToNextCurve<55+state.getCurrentSpeed()*3){
			newThrottle = changeThrottle(newThrottle, deltaSpeed);
		}else{
			newThrottle = 1;
		}
		
		newThrottle = limitThrottle(newThrottle);
		
		/*if(Math.abs(state.getCurrentAngle())>8){
			newThrottle *= 1.0-Math.abs(state.getCurrentAngle())*0.01;
		}
		*/
		
		if(distToNextCurve>400){
			Turbo turbo = state.getAndUseTurbo();
			if(turbo != null){
				return new DrivingCommand(CommandType.UseTurbo); 
			}
		}
		
		DrivingCommand command = new DrivingCommand(CommandType.Throttle);
		command.setThrottleValue(newThrottle);
		
		logger.info("\n current speed = "+state.getCurrentSpeed()+"\n optimal speed = "+optimalSpeed+"\n distance to curve = "+distToNextCurve+"\n\n");
		
		return command;
	}
	public DrivingCommand getCommand(State state) {
		return testDrive(state);
	}
}
