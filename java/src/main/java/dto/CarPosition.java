package dto;

public class CarPosition {
	
	public double angle;
	
	public PiecePosition piecePosition;
	public PositionId id;
	
	public class PositionId{
		public String name;
		public String color;
	}
	
	public class PiecePosition{
		public double inPieceDistance;
		public int pieceIndex;
		public int lap;
		
		public LaneMove lane;
		
		public class LaneMove{
			public int startLaneIndex;
			public int endLaneIndex;
		}
	}
	
}
