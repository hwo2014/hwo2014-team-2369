package dto;

import java.util.List;

public class CarPositions {
	public int gameTick;
	public String gameId;
	public List<CarPosition> data;
}
