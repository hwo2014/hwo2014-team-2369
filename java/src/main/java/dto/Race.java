package dto;

import java.util.List;


public class Race {
	public Track track;
	public List<Car> cars; 
	public RaceSession raceSession;
	
	public Race(){
		
	}
	
	public class RaceSession{
		public int laps;
		public int maxLapTimeMs;
		
		public boolean quickRace;
	}
}
