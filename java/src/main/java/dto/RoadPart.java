package dto;

import com.google.gson.annotations.SerializedName;

public class RoadPart {
	public double coordinate;
	public double length;
	@SerializedName("switch")
	public boolean switchEnabled;
	public double angle;
	public double radius;
}
