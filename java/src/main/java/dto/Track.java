package dto;

import java.awt.Point;
import java.util.*;

public class Track {
	
	public List<RoadPart> pieces;	//List of road pieces
	public List<Lane> lanes;	//list of lanes
	public String id;	//unique id
	public String name;
	public StartingPoint startingPoint;
	public double trackLength;
	
	public Track(){
	}
	
	public class StartingPoint{
		Point position;
		double angle;
	}
	
}
