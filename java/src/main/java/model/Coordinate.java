package model;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Coordinate {
	private Map<Integer, Double> laneValues;
	
	public Coordinate() {
		laneValues = new HashMap<Integer, Double>();
	}
	
	public Coordinate(Set<Integer> laneIndexes) {
		laneValues = new HashMap<Integer, Double>();
		for(Integer lane : laneIndexes) {
			laneValues.put(lane, 0d);
		}
	}
	
	private Coordinate(Map<Integer, Double> laneValues) {
		this.laneValues = laneValues;
	}
	
	public void setLaneValue(int laneIndex, double value) {
		laneValues.put(laneIndex, value);
	}
	
	public double getValue(int laneIndex) {
		return laneValues.get(laneIndex);
	}
	
	public Set<Integer> getLanes() {
		return laneValues.keySet();
	}
	
	public Coordinate add(Coordinate other) {
		HashMap<Integer, Double> resMap = new HashMap<Integer, Double>();
		for(Integer lane : laneValues.keySet()) {
			resMap.put(lane, this.getValue(lane) + other.getValue(lane));
		}
		Coordinate result = new Coordinate(resMap);
		return result;
	}
}
