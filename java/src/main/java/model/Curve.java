package model;

import java.util.Map;

public class Curve extends RacePart {

	private double radius;
	private double angle;
	private Map<Integer, Double> laneOffsets;
	
	public Curve(double radius, double angle, boolean isSwitch, Coordinate startCoordinate, Map<Integer, Double> laneOffsets, int index) {
		super(isSwitch, startCoordinate, index);
		this.radius = radius;
		this.angle = angle;
		this.laneOffsets = laneOffsets;		
	}
	
	
	@Override
	public RoadType getType() {
		return RoadType.Curve;
	}

	@Override
	public double getLength(int laneIndex) {
		 return Math.abs((radius - Math.signum(angle)*laneOffsets.get(laneIndex))*(angle/180*Math.PI));
	}

	@Override
	public Coordinate getLength() {
		Coordinate result = new Coordinate();
		for(Integer lane : laneOffsets.keySet()) {
			result.setLaneValue(lane, getLength(lane));
		}
		
		return result;
	}
	
	public double getRadius(int laneIndex){
		return radius - Math.signum(angle) * laneOffsets.get(laneIndex);
	}

}
