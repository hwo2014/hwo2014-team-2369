package model;

public class DrivingCommand {
	
	public enum CommandType{
		Throttle,
		LaneChange,
		UseTurbo
	}
	
	public enum LaneChageDirection {
		LEFT,
		RIGHT
	}
	
	private double throttleValue;
	private LaneChageDirection laneChange;
	public CommandType type;
	
	//When creating driving command, have to also initialize command type
	public DrivingCommand(CommandType type){
		this.type = type;
	}

	public double getThrottleValue() {
		return throttleValue;
	}

	public void setThrottleValue(double throttleValue) {
		if(throttleValue > 1) {
			this.throttleValue = 1;
		} else if(throttleValue < 0) {
			this.throttleValue = 0;
		} else {
			this.throttleValue = throttleValue;
		}
	}
	
	public LaneChageDirection getLaneChange() {
		return laneChange;
	}

	public void setLaneChange(LaneChageDirection laneChange) {
		this.laneChange = laneChange;
	}
}
