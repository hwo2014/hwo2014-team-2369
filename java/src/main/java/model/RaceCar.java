package model;

public class RaceCar {
	private String name;
	private String color;
	
	private double width;
	private double length;
	private double guidePosition;
	
	private double position;
	private double speed;
	private double acceleration;
	
	private double angle;
	private double angleDelta;
	private double angleAcceleration;
	
	private int lane;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
	}
	public double getGuidePosition() {
		return guidePosition;
	}
	public void setGuidePosition(double guidePosition) {
		this.guidePosition = guidePosition;
	}
	public double getPosition() {
		return position;
	}
	public void setPosition(double position) {
		this.position = position;
	}
	public double getAngle() {
		return angle;
	}
	public void setAngle(double angle) {
		this.angle = angle;
	}
	public double getAngleDelta() {
		return angleDelta;
	}
	public void setAngleDelta(double angleDelta) {
		this.angleDelta = angleDelta;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	public double getAcceleration() {
		return acceleration;
	}
	public void setAcceleration(double acceleration) {
		this.acceleration = acceleration;
	}
	public int getLane() {
		return lane;
	}
	public void setLane(int lane) {
		this.lane = lane;
	}
	public double getAngleAcceleration() {
		return angleAcceleration;
	}
	public void setAngleAcceleration(double angleAcceleration) {
		this.angleAcceleration = angleAcceleration;
	}
}
