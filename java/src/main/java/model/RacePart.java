package model;

public abstract class RacePart {
	public enum RoadType{
		Straight,
		Curve
	}
	private int index;
	private boolean isSwitch;
	private Coordinate startCoordinate;

	RacePart(boolean isSwitch, Coordinate startCoordinate, int index) {
		this.isSwitch = isSwitch;
		this.startCoordinate = startCoordinate;
		this.index = index;
	}
	
	public boolean isSwitch() {
		return isSwitch;
	}

	public int getIndex() {
		return index;
	}
	
	public double getStartCoordinate(int laneIndex) {
		return this.startCoordinate.getValue(laneIndex);
	}
	public abstract RoadType getType();
	
	public abstract double getLength(int laneIndex);
	
	public abstract Coordinate getLength();
}