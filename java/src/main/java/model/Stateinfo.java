package model;

public class Stateinfo {
	public int tick;
	public int pieceIndex;
	public double angle;
	public double throttle;
	public double position;
	public double speed;
	public double rad_acceleration;
	public double angleDelta;
	public double angleAccel;
	public double targetSpeed;
	public int lane;
}