package model;

import java.util.Map;

public class StraightRoad extends RacePart {
	private double length;
	private Map<Integer, Double> laneOffsets;
	public StraightRoad(double length, boolean isSwitch, Coordinate startCoordinates, Map<Integer, Double> laneOffsets, int index) {
		super(isSwitch, startCoordinates, index);
		this.length = length;
		this.laneOffsets = laneOffsets;
	}
	@Override
	public RoadType getType() {
		return RoadType.Straight;
	}
	@Override
	public double getLength(int laneIndex) {
		return length;
	}
	@Override
	public Coordinate getLength() {
		Coordinate result = new Coordinate();
		for(Integer lane : laneOffsets.keySet()) {
			result.setLaneValue(lane, length);
		}
		return result;
	}

}
