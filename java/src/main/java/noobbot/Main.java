package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;

import model.DrivingCommand;
import model.DrivingCommand.CommandType;
import model.DrivingCommand.LaneChageDirection;
import model.Stateinfo;
import model.Turbo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import driver.*;
import dto.*;
import requests.*;

public class Main {
	
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        String trackName = null;
        if(args.length > 4) {
        	trackName = args[4];
        }

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(botName, reader, writer, new Join(botName, botKey), trackName);
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    private State state;
    private Logger logger = LoggerFactory.getLogger("Bot.main");
    
    public Main(final String botName, final BufferedReader reader, final PrintWriter writer, final Join join, final String trackName) throws IOException {
        this.writer = writer;
        String line = null;

        if(trackName != null) {
        	send(new JoinRaceMsg(join, trackName, "", 1));
        } else {
        	send(join);
        }
        Driver driver = null;
        state = new State();
        while((line = reader.readLine()) != null) {
                	
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
            	CarPositions newPositions = gson.fromJson(line, CarPositions.class);
            	state.updateCarPositions(newPositions);
            	DrivingCommand command = driver.getCommand(state);
            	if(command != null) {
            		if(command.type == CommandType.Throttle) {
		            	state.updateThrottle(command.getThrottleValue());
		            	send(new Throttle(command.getThrottleValue()));
            		} else if(command.type == CommandType.LaneChange){
            			if(command.getLaneChange().equals(LaneChageDirection.RIGHT)) {
            				send(new LaneSwitch("Right"));
            			} else {
            				send(new LaneSwitch("Left"));
            			}
            		} else if(command.type == CommandType.UseTurbo){
            			send(new UseTurbo("Go go!"));
            		}
            	} else {
            		// No command -> send ping
            		send(new Ping());
            	}
            	state.logState();
            } else if (msgFromServer.msgType.equals("join")) {
                logger.debug("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
            	logger.debug("Race init"); 	
                Game game = gson.fromJson(gson.toJson(msgFromServer.data), Game.class);
                state.init(game);
                if(driver == null) {
                	driver = new Driver(state);
                }
                state.setDriver(driver);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
            	logger.debug("Race end");
            	send(new Ping());
            } else if (msgFromServer.msgType.equals("gameStart")) {
            	logger.debug("Race start");
            	send(new Ping());
            } else if(msgFromServer.msgType.equals("crash")) {
            	if(state.updateCrash(msgFromServer)) {
            		driver.crashed(state);
            	}
            	send(new Ping());
            } else if(msgFromServer.msgType.equals("spawn")) {
            	state.updateCrash(msgFromServer);
            	send(new Ping());
            } else if(msgFromServer.msgType.equals("yourCar")) {
            	String carColor = (String) ((Map)msgFromServer.data).get("color");
            	logger.debug("Car color: " + carColor);
            	state.setOwnCarColor(carColor);
            	send(new Ping());
            } else if(msgFromServer.msgType.equals("turboAvailable")) {
            	Turbo turbo = gson.fromJson(gson.toJson(msgFromServer.data), Turbo.class);
            	logger.debug("Turbo available!");
            	state.setTurboAvailable(turbo);
            	send(new Ping());
            } else if(msgFromServer.msgType.equals("turboEnd")) {
            	state.setTurboEnd(msgFromServer);
            } else {
                send(new Ping());
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}
