package noobbot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import requests.MsgWrapper;

import com.google.gson.Gson;

import model.Coordinate;
import model.Curve;
import model.RaceCar;
import model.RacePart;
import model.Stateinfo;
import model.StraightRoad;
import model.Turbo;
import model.RacePart.RoadType;
import driver.Driver;
import dto.Car;
import dto.CarPosition;
import dto.CarPositions;
import dto.Game;
import dto.Lane;
import dto.Race;
import dto.RoadPart;

public class State {
	private List<RacePart> track;
	private Map<Integer, Double> laneOffsets;
	private List<RaceCar> competitors;
	private RaceCar ownCar;
	private String ownCarColor;
	private Coordinate trackLength;
	private int currentTick;
	private double currentThrottleValue = 0;
	private Turbo turbo = null;
	private boolean isCrashed;
    private final Gson gson = new Gson();
    private Driver driver;
    private Logger logger = LoggerFactory.getLogger("Bot.state");

	public void init(Game game) {
		this.competitors = new ArrayList<RaceCar>();
		for(Car car : game.race.cars) {
			RaceCar rcar = createRaceCar(car);
			if(car.id.color.equals(ownCarColor)) {
				ownCar = rcar;
			} else {
				competitors.add(rcar);
			}
		}
		
		laneOffsets = new HashMap<Integer, Double>();
		for(Lane lane : game.race.track.lanes) {
			laneOffsets.put(lane.index, lane.distanceFromCenter);
		}
		
		track = new ArrayList<RacePart>();
		Coordinate currentCoordinate = new Coordinate(laneOffsets.keySet());
		int partIndex = 0;
		for(RoadPart part : game.race.track.pieces) {			
			if(part.angle != 0) {
				Curve c = new Curve(part.radius, part.angle, part.switchEnabled, currentCoordinate, laneOffsets, partIndex);
				currentCoordinate = currentCoordinate.add(c.getLength());
				track.add(c);
				
				/*String log = "curve: \n";
				for(int l=0; l<laneOffsets.size(); l++){
					log += "lane"+l+" length = "+c.getLength(l)+", startCoord = "+c.getStartCoordinate(l)+"\n";
				}
				logger.info(log);*/
			} else {
				StraightRoad r = new StraightRoad(part.length, part.switchEnabled, currentCoordinate, laneOffsets, partIndex);
				currentCoordinate = currentCoordinate.add(r.getLength());
				track.add(r);
				
				/*String log = "straight: \n";
				for(int l=0; l<laneOffsets.size(); l++){
					log += "lane"+l+" length = "+r.getLength(l)+", startCoord = "+r.getStartCoordinate(l)+"\n";
				}
				logger.info(log);*/
			}
			partIndex++;
		}
		trackLength = currentCoordinate;
		logger.info("Lanes:");
		for(Integer lane : laneOffsets.keySet()) {
			logger.info(" " + lane + " " + laneOffsets.get(lane));
		}
		logger.info("Track:");
		for(RacePart part : track) {
			if(part.getType().equals(RoadType.Curve)) {
				Curve c = (Curve)part;
				logger.info("Part " + c.getIndex() + ": type=" + c.getType() + " isSwitch=" + c.isSwitch() + " radius(0)=" + c.getRadius(0) + " length(0)=" + c.getLength(0));

			} else {
				logger.info("Part " + part.getIndex() + ": type=" + part.getType() + " isSwitch=" + part.isSwitch());
			}
			for(Integer lane : laneOffsets.keySet()) {
				double len = part.getStartCoordinate(lane) + part.getLength(lane);
				logger.info("    " + lane + ": " + part.getStartCoordinate(lane) + " - " + part.getLength(lane) +  " - " + len);
			}
		}
	}
	
	public void setTurboAvailable(Turbo turbo){
		if(!isCrashed) {
			this.turbo = turbo;
		}
	}
	
	public int getLaneCount(){
		return laneOffsets.size();
	}
	
	public Turbo getAndUseTurbo(){
		Turbo result = turbo;
		turbo = null;
		return result;
	}
	
	public void setDriver(Driver driver) {
		this.driver = driver;
	}
	
	public boolean updateCrash(MsgWrapper message) {
		if(message.msgType.equals("crash")) {
			if( ((Map)message.data).get("color").equals(ownCarColor) ) {
            	logger.warn("Crash");
				isCrashed = true;
				currentThrottleValue = 0;
				return true;
			}
		} else if(message.msgType.equals("spawn")) {
			if( ((Map)message.data).get("color").equals(ownCarColor) ) {
            	logger.warn("Spawn");
				isCrashed = false;
				return true;
			}
		}
		return false;
	}
	
	public void setOwnCarColor(String ownCarColor) {
		this.ownCarColor = ownCarColor;
	}
	
	public void updateThrottle(double throttleValue) {
		this.currentThrottleValue = throttleValue;
	}
	
	public double getThrottleValue() {
		return currentThrottleValue;
	}
	
	public void updateCarPositions(CarPositions positions) {
		for(CarPosition position : positions.data) {
			boolean changingLanes = position.piecePosition.lane.endLaneIndex != position.piecePosition.lane.startLaneIndex;
			int laneIndex = position.piecePosition.lane.endLaneIndex;
			double posValue = getPosition(position.piecePosition.pieceIndex, position.piecePosition.inPieceDistance, laneIndex);
			double angle = position.angle;

			RaceCar rcar = getCar(position.id.color);
			// Speed / tick = old position - new position
			if(positions.gameTick == 0) {
				rcar.setSpeed(0);
			} else if(changingLanes) {
				// if changing lanes, position values won't be
				// comparable, just use previous speed here
			} else if(posValue >= rcar.getPosition()) {
				double newSpeed = posValue - rcar.getPosition();
				double newAcceleration = newSpeed - rcar.getSpeed();
				rcar.setSpeed(newSpeed);
				rcar.setAcceleration(newAcceleration);
			} else {
				// If something is weird (lap change!) use previous speed value
				// this should be accurate enough
				rcar.setSpeed(rcar.getSpeed());
				// TODO this might cause problems??
				rcar.setAcceleration(0);
			}
			rcar.setPosition(posValue);
			if(positions.gameTick == 0) {
				rcar.setAngleDelta(0);
				rcar.setAngleAcceleration(0);
			} else {
				double angleDelta = angle - rcar.getAngle();
				double angleAcceleration = angleDelta - rcar.getAngleDelta();		
				rcar.setAngleDelta(angleDelta);
				rcar.setAngleAcceleration(angleAcceleration);
			}
			rcar.setAngle(angle);
			rcar.setLane(laneIndex);
		}
		this.currentTick = positions.gameTick;
	}

	public RacePart getRacePart(int index) {
		return track.get(index % track.size());
	}
	
	public int getNextSwitchIndex(int startingIndex) {
		for(int n=startingIndex; n<track.size()*2; n++) {
			RacePart part = track.get(n % track.size());
			if(part.isSwitch()) {
				return n% track.size();
			}
		}
		return -1;
	}
	/* Distance from between two pieces (inclusive) */
	public Coordinate getDistance(int startPieceIndex, int endPieceIndex) {
		Coordinate result = new Coordinate(laneOffsets.keySet());
		int end = endPieceIndex;
		// Crosses start line
		if(endPieceIndex < startPieceIndex) {
			end = track.size();
		}
		
		for(int i=startPieceIndex; i<end; i++) {
			result = result.add(track.get(i).getLength());
		}
		
		if(endPieceIndex < startPieceIndex) {
			for(int i=0; i<= endPieceIndex; i++) {
				result = result.add(track.get(i).getLength());
			}
		}
		return result;
	}
	
	public RacePart getCurrentRacePart() {
		int currentPartIndex = getPartIndex(ownCar.getPosition(), ownCar.getLane());
		return track.get(currentPartIndex);
	}

	public Curve getNextCurve(){
		//loop twice all pieces, so that also pieces behind the given coordinate would be checked
		int currentPartIndex = getPartIndex(ownCar.getPosition(), ownCar.getLane());
		for(int n=currentPartIndex; n<track.size()*2; n++){
			RacePart part = track.get(n % track.size());
			if(part.getType() == RacePart.RoadType.Curve){
				return (Curve)part;
			}
		}
		return null;
	}
	
	//Get number of curves in row
	public ArrayList<Curve> getNextCurves(int number){
		
		ArrayList<Curve> curves = new ArrayList<Curve>();
		boolean curveBegan = false;
		//loop twice all pieces, so that also pieces behind the given coordinate would be checked
		int currentPartIndex = getPartIndex(ownCar.getPosition(), ownCar.getLane());
		for(int n=currentPartIndex; n<track.size()*2 && curves.size()<=number; n++){
			RacePart part = track.get(n % track.size());
			if(part.getType() == RacePart.RoadType.Curve){
				curves.add((Curve)part);
				curveBegan = true;
			}else{
				if(curveBegan){
					break;
				}
			}
		}
		return curves;
	}
	
	public double getDistanceToNextCurve(){
		
		RacePart part = getNextCurve();
		int laneIndex = ownCar.getLane();
		double carCoordinate = ownCar.getPosition();
		double result = part.getStartCoordinate(laneIndex) - carCoordinate;
		
		//if result is smaller than zero, it would mean that coordinate system has rolled over
		//or the car is currently in the curve part.
		if(result<0){
			
			// if car is currently in the curve part, return zero.
			if(getCurrentRacePart().getType().equals(RacePart.RoadType.Curve)){
				return 0;
			}else{
				//coordinate system has rolled over, so have to increase result with trackLength
				return result + trackLength.getValue(laneIndex);
			}

		}else{
			
			//result is bigger than zero, so it's valid distance
			return result;
			
		}
		
	}
	
	public int getTrackLength() {
		return track.size();
	}
	
	public int getCurrentTick() {
		return currentTick;
	}
	
	public double getCurrentSpeed() {
		return ownCar.getSpeed();
	}
	
	public double getCurrentAcceleration() {
		return ownCar.getAcceleration();
	}
	
	public double getCurrentAngle() {
		return ownCar.getAngle();
	}
	
	public double getCurrentAngleDelta() {
		return ownCar.getAngleDelta();
	}
	
	public double getCurrentAngleAcceleration() {
		return ownCar.getAngleAcceleration();
	}
	
	
	public double getCurrentRadAcceleration() {
		double result;
		if(getCurrentRacePart().getType().equals(RacePart.RoadType.Curve)) {
			Curve currentCurve = getNextCurve();
			result = (getCurrentSpeed() * getCurrentSpeed()) / currentCurve.getRadius(ownCar.getLane());
		} else {
			result = 0;
		}
		return result;
	}
	
	public int getCurrentLane() {
		return ownCar.getLane();
	}
	
	public double getLaneOffset(int laneIndex) {
		return laneOffsets.get(laneIndex);
	}
	
	public void logState() {
    	logger.info("State=" + gson.toJson(getState()));
	}
	
	private Stateinfo getState() {
		Stateinfo result = new Stateinfo();
		result.angle = ownCar.getAngle();
		result.speed = ownCar.getSpeed();
		result.targetSpeed = driver.getTargetSpeed();
		result.position = ownCar.getPosition();
		result.throttle = currentThrottleValue;
		result.lane = ownCar.getLane();
		result.tick = currentTick;
		result.pieceIndex = getCurrentRacePart().getIndex();
		result.rad_acceleration = getCurrentRadAcceleration();
		result.angleDelta = getCurrentAngleDelta();
		result.angleAccel = getCurrentAngleAcceleration();
		
		return result;
	}
	
	
	public boolean isCrashed() {
		return isCrashed;
	}

	private RaceCar createRaceCar(Car in) {
		RaceCar rcar = new RaceCar();
		rcar.setName(in.id.name);
		rcar.setColor(in.id.color);
		rcar.setWidth(in.dimensions.width);
		rcar.setLength(in.dimensions.length);
		rcar.setGuidePosition(in.dimensions.guideFlagPosition);
		return rcar;
	}
	
	private RaceCar getCar(String color) {
		if(this.ownCarColor.equals(color)) {
			return ownCar;
		} else {
			for(RaceCar car : competitors) {
				if(car.getColor().equals(color)) {
					return car;
				}
			}
		}
		return null;
	}
	
	public List<RaceCar> getAllCars(){
		return competitors;
	}
	
	public RaceCar getOwnCar(){
		return ownCar;
	}
	
	public int getPartIndex(double position, int laneIndex) {
		for(int i=1; i<track.size(); i++) {
			if(track.get(i).getStartCoordinate(laneIndex) > position) {
				return i-1;
			}
		}
		return track.size()-1;
	}
	
	private double getPosition(int partIndex, double pieceDistance, int laneIndex) {
		return track.get(partIndex).getStartCoordinate(laneIndex) + pieceDistance;
	}

	public void setTurboEnd(MsgWrapper msgFromServer) {
		if( ((Map)msgFromServer.data).get("color").equals(ownCarColor) ){
			logger.info("Turbo ended");
			driver.setTurboEnded();
		}
	}
}
