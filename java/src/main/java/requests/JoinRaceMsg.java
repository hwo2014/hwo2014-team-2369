package requests;

public class JoinRaceMsg extends SendMsg{
	Join botId;
	String trackName;
	String password;
	int carCount;
	public JoinRaceMsg(Join botId, String trackName, String password, int carCount){
		this.botId = botId;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}
	
	public JoinRaceMsg(Join botId, String password, int carCount){
		this.botId = botId;
		this.password = password;
		this.carCount = carCount;
	}
	
	@Override
    protected Object msgData() {
        return this;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}