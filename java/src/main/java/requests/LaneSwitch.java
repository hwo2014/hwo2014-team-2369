package requests;

public class LaneSwitch extends SendMsg {
	private String direction;
	
	public LaneSwitch(String direction) {
		this.direction = direction;
	}
	
	@Override
	protected String msgType() {
		return "switchLane";
	}
	
    @Override
    protected Object msgData() {
        return direction;
    }
}
