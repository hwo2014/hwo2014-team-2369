#!/usr/bin/python3

import json
import numpy as np
import matplotlib.pyplot as plt
import sys


def plot(infile):
  f = open(infile, 'r')
  strin = f.readlines()
  f.close()

  plotdata = []
  for line in strin:
    if "Bot.state - State" in line:
      data = line.split(' - ')[1].split('=')[1]
      state = json.loads(data)
      plotdata.append(state)
  fig, pl1 = plt.subplots()
  
  pl1.plot(list(a['angle'] for a in plotdata), 'b')
  pl1.plot(list(a['speed'] for a in plotdata), 'y')
  pl1.plot(list(a['rad_acceleration']*20 for a in plotdata), 'k')
  
  pl2 = pl1.twinx()
  pl2.plot(list(a['throttle'] for a in plotdata), 'r')
  pl2.set_ylim((-1.1,1.1))
  for tl in pl2.get_yticklabels():
    tl.set_color('r')

  plt.show()
  
  
if __name__ == "__main__":
    plot(sys.argv[1])